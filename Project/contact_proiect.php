	<?php
	include ("/elements/html1_proiect.php");
	?>

	
	<?php
	include ("/elements/proiect_header.php");
	?>

	<div class="container-fluid sectiune2_contact">
		<div class="opac_contact"></div>
		<div class="container pos_relative">
			<div class="row">
				<div class="titlu titlu_about">
					Get in Touch
				</div>
				<div class="line2 line3"></div>
				<div class="col-md-4 sect2_about_text">
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
			</div>
		</div>
		<?php
			include ("/elements/meniu.php");
		?>
	</div>
	<div class="clear"></div>
	<div class="container-fluid sectiune3_contact">
		<div class="row">
			<div class="col-md-3 co pos_relative sect3_col">
				<div>
					55 Cyan Avenue, Suite 65
				</div>
				<div>
					Los Angeles, CA 8008
				</div>
				<div class="sect3_contact_i">
					<i class="fa fa-map-marker"></i>
				</div>
			</div>
			<div class="col-md-3 cn pos_relative sect3_col">
				<div>
					support@palas.com
				</div>
				<div>
					billing@palas.com
				</div>
				<div class="sect3_contact_i">
					<i class="fa fa-envelope"></i>
				</div>
			</div>
			<div class="col-md-3 clb pos_relative sect3_col">
				<div>
					0 800-55-22-55
				</div>
				<div>
					0 800-22-44-55
				</div>
				<div class="sect3_contact_i">
					<i class="fa fa-phone"></i>
				</div>
			</div>
			<div class="col-md-3 cp pos_relative sect3_col">
				<div>
					Mon-Fri: 09:00-18:00
				</div>
				<div>
					Sat-Sun: Closed
				</div>
				<div class="sect3_contact_i">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune4_contact">
		<div class="container">
			<div class="row">
				<div class="col-md-7 sect4_contact_input">
					<form>
						<input type="text" placeholder="First Name" name="first_name" class="sect4_contact_left prenume">
						<input type="text" placeholder="Last Name" name="last_name" class="sect4_contact_right nume">
						<input type="text" placeholder="Subject*" name="subject" class="sect4_contact_left subject">
						<input type="email" placeholder="Email Address*" name="email" class="sect4_contact_right email_contact">
						<textarea class="sect4_contact_textarea textarea" placeholder="Message*"></textarea>
						<div class="checkbox_contact">
							<input type="checkbox" name="checkbox" value=""> Nu sunt robot
						</div>
						<input type="submit" value="SUBMIT" class="submit_checkbox cn">
					</form>
				</div>
				<div class="col-md-5">
					<div class="titlu">
						Contact us
					</div>
					<div class="line2 line3"></div>
					<div>
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
					<div class="titlu margin_top">
						Get social
					</div>
					<div class="line2 line3"></div>
					<div class="col-md-1 no_padding">
						<a href="">
							<i class="fa fa-twitter"></i>
						</a>
					</div>
					<div class="col-md-5 sect4_contact_social_1 no_padding">
						Follow us
					</div>
					<div class="clear"></div>
					<div class="col-md-1 no_padding">
						<a href="">
							<i class="fa fa-facebook-f"></i>
						</a>
					</div>
					<div class="col-md-5 sect4_contact_social_2 no_padding">
						Like us
					</div>
					<div class="clear"></div>
					<div class="col-md-1 no_padding">
						<a href="">
							<i class="fa fa-envelope"></i>
						</a>
					</div>
					<div class="col-md-5 sect4_contact_social_3 no_padding">
						Subscribe
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune5_contact">
		<div class="row">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d86818.84040676105!2d27.51693037640556!3d47.15611595585052!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40cafb7cf639ddbb%3A0x7ccb80da5426f53c!2zSWHImWk!5e0!3m2!1sro!2sro!4v1523896784410" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>


	<?php
	include ("/elements/proiect_footer.php");
	?>
	
	<?php
	include ("/elements/html2_proiect.php");
	?>