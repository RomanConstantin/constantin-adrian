	<?php
	include ("/elements/html1_proiect.php");
	?>

	
	<?php
	include ("/elements/proiect_header.php");
	?>

	<div class="container-fluid sectiune2_about">
		<div class="opac_404"></div>
		<div class="container pos_relative">
			<div class="row">
				<div class="titlu titlu_about">
					About us
				</div>
				<div class="line2 line3"></div>
				<div class="col-md-4 sect2_about_text">
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
				<div class="short_meniu">
					<div class="float_left">
						<a href="">HOME</a>
					</div>
					<div class="float_left">
						/
					</div>
					<div class="float_left">
						<a href="">PAGES</a>
					</div>
					<div class="float_left">
						/
					</div>
					<div class="float_left">
						<a href="">ABOUT</a>
					</div>
				</div>
			</div>
		</div>
		<?php
			include ("/elements/meniu.php");
		?>
	</div>
	<div class="clear"></div>
	<div class="container-fluid sectiune3_about">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="text-center sect3_img">
						<i class="fa fa-globe"></i>
					</div>
					<div class="text-center subtitlu">
						Everywhere
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
				</div>
				<div class="col-md-4">
					<div class="text-center sect3_img">
						<i class="fa fa-exclamation"></i>
					</div>
					<div class="text-center subtitlu">
						Reliable
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
				</div>
				<div class="col-md-4">
					<div class="text-center sect3_img">
						<i class="fa fa-comments"></i>
					</div>
					<div class="text-center subtitlu">
						We help you
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune4_about">
		<div class="row">
			<div class="col-md-4 sect4_about1">
				<div class="fade1"></div>
				<div class="titlu ">
					EMPOWERING
				</div>
				<div class="line2 line3"></div>
				<div>
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
			</div>
			<div class="col-md-4 sect4_about2">
				<div class="fade1"></div>
				<div class="titlu ">
					INSPIRING
				</div>
				<div class="line2 line3"></div>
				<div>
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
			</div>
			<div class="col-md-4 sect4_about3">
				<div class="fade1"></div>
				<div class="titlu ">
					REWARDING
				</div>
				<div class="line2 line3"></div>
				<div>
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune5_about">
		<div class="container">
			<div class="row">
				<div class="col-md-6 sect5_about_img"></div>
				<div class="col-md-6">
					<div class="sect5_about_part2">
						<div class="col-md-2 sect5_about_icon">
							<i class="fa fa-briefcase"></i>
						</div>
						<div class="col-md-10">
							<div class="subtitlu">
								Work Hard
							</div>
							<div>
								Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div>
						<div class="col-md-2 sect5_about_icon">
							<i class="fa fa-heart"></i>
						</div>
						<div class="col-md-10">
							<div class="subtitlu">
								Play Hard
							</div>
							<div>
								Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune6_about">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="text-center sect5_about_icon">
						<i class="fa fa-copy"></i>
					</div>
					<div class="text-center sect6_about_text">
						123
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Web Projects
					</div>
				</div>
				<div class="col-md-3">
					<div class="text-center sect5_about_icon">
						<i class="fa fa-thumbs-up"></i>
					</div>
					<div class="text-center sect6_about_text">
						1850
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Happy Customers
					</div>
				</div>
				<div class="col-md-3">
					<div class="text-center sect5_about_icon">
						<i class="fa fa-life-ring"></i>
					</div>
					<div class="text-center sect6_about_text">
						1756
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Support Tickets
					</div>
				</div>
				<div class="col-md-3">
					<div class="text-center sect5_about_icon">
						<i class="fa fa-users"></i>
					</div>
					<div class="text-center sect6_about_text">
						58
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Employees
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune7_about">
		<div class="fade2"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<img src="/web/proiect/img/client01a.png">
				</div>
				<div class="col-md-3">
					<img src="/web/proiect/img/client02a.png">
				</div>
				<div class="col-md-3">
					<img src="/web/proiect/img/client03a.png">
				</div>
				<div class="col-md-3">
					<img src="/web/proiect/img/client04a.png">
				</div>
				<div class="col-md-3">
					<img src="/web/proiect/img/client05a.png">
				</div>
				<div class="col-md-3">
					<img src="/web/proiect/img/client06a.png">
				</div>
				<div class="col-md-3">
					<img src="/web/proiect/img/client07a.png">
				</div>
				<div class="col-md-3">
					<img src="/web/proiect/img/client08a.png">
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune8_about">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="margin_bottom">
						<div class="col-md-1">
							<i class="fa fa-angle-up"></i>
						</div>
						<div class="subtitlu">
							How to create an account
						</div>
					</div>
					<div class="col-md-offset-1 margin_bottom">
						Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
					</div>
					<div class="margin_bottom">
						<div class="col-md-1">
							<i class="fa fa-angle-up"></i>
						</div>
						<div class="subtitlu">
							How to change username
						</div>
					</div>
					<div class="margin_bottom">
						<div class="col-md-1">
							<i class="fa fa-angle-down"></i>
						</div>
						<div class="subtitlu">
							Account removal
						</div>
					</div>
					<div class="margin_bottom">
						<div class="col-md-1">
							<i class="fa fa-angle-down"></i>
						</div>
						<div class="subtitlu">
							Forgot password
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="subtitlu">
						Why choose us
					</div>
					<div class="line2 line3"></div>
					<div class="margin_bottom">
						Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
					</div>
					<div>
						<div class="col-md-1 sect8">
							<b><i class="fa fa-life-ring"></i></b>
						</div>
						<div class="col-md-11 subtitlu sect8_titlu">
							<b>PREMIUM SUPPORT</b>
						</div>
					</div>
					<div>
						Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
	include ("/elements/proiect_footer.php");
	?>
	
	<?php
	include ("/elements/html2_proiect.php");
	?>