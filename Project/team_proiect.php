	<?php
	include ("/elements/html1_proiect.php");
	?>

	

	<?php
	include ("/elements/proiect_header.php");
	?>

	<div class="container-fluid sectiune2_faq">
		<div class="opac_contact"></div>
		<div class="container pos_relative">
			<div class="row">
				<div class="titlu titlu_about">
					Our Team
				</div>
				<div class="line2 line3"></div>
				<div class="col-md-4 sect2_about_text">
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
				<div class="float_right portofolio_meniu">
					<div class="sect7_right float_left">
						<a href="">Home</a>
					</div>
					<div class="sect7_right float_left" style="color: #667;">
						/
					</div>
					<div class="sect7_right float_left">
						<a href="">Pages</a>
					</div>
					<div class="sect7_right float_left" style="color: #667;">
						/
					</div>
					<div class="sect7_right float_left">
						<a href="">Team</a>
					</div>
				</div>
			</div>
		</div>

		<?php
			include ("/elements/meniu.php");
		?>

	</div>
	<div class="clear"></div>
	<div class="popup pos_relative">
		<div class="opac_contact"></div>
		<div class="pos_relative popup1"></div>
		<div class="close-this"> 
			<i class="fa fa-times"></i> 
		</div>
	</div>
	<div class="popup2 pos_relative">
		<div class="opac_contact"></div>
		<div class="pos_relative popup_2"></div>
		<div class="close-this"> 
			<i class="fa fa-times"></i> 
		</div>
	</div>
	<div class="popup3 pos_relative">
		<div class="opac_contact"></div>
		<div class="pos_relative popup_3"></div>
		<div class="close-this"> 
			<i class="fa fa-times"></i> 
		</div>
	</div>
	<div class="container-fluid sectiune2_team">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div data-value="popup" class="sect2_team_img1 margin_bottom"></div>
					<div>
						<div class="float_left subtitlu">
							John Doe
						</div>
						<div class="float_right">
							<div class="float_left">
								<a href="">
									<i class="fa fa-twitter"></i>
								</a>
							</div>
							<div class="float_left">
								<a href="">
									<i class="fa fa-dribbble"></i>
								</a>
							</div>
							<div class="float_left">
								<a href="">
									<i class="fa fa-behance"></i>
								</a>
							</div>
						</div>
						<div class="clear"></div>
						<div class="fco margin_bottom">
							web designer
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div data-value="popup2" class="sect2_team_img2 margin_bottom"></div>
					<div>
						<div class="float_left subtitlu">
							Mark Doe
						</div>
						<div class="float_right">
							<div class="float_left">
								<a href="">
									<i class="fa fa-twitter"></i>
								</a>
							</div>
							<div class="float_left">
								<a href="">
									<i class="fa fa-dribbble"></i>
								</a>
							</div>
							<div class="float_left">
								<a href="">
									<i class="fa fa-behance"></i>
								</a>
							</div>
						</div>
						<div class="clear"></div>
						<div class="fco margin_bottom">
							web designer
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div data-value="popup3" class="sect2_team_img3 margin_bottom"></div>
					<div>
						<div class="float_left subtitlu">
							Alex Doe
						</div>
						<div class="float_right">
							<div class="float_left">
								<a href="">
									<i class="fa fa-twitter"></i>
								</a>
							</div>
							<div class="float_left">
								<a href="">
									<i class="fa fa-dribbble"></i>
								</a>
							</div>
							<div class="float_left">
								<a href="">
									<i class="fa fa-behance"></i>
								</a>
							</div>
						</div>
						<div class="clear"></div>
						<div class="fco margin_bottom">
							manager
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="margin_bottom"></div>
				<div class="col-md-4">
					<div data-value="popup" class="sect2_team_img1 margin_bottom"></div>
					<div>
						<div class="float_left subtitlu">
							John Doe
						</div>
						<div class="float_right">
							<div class="float_left">
								<a href="">
									<i class="fa fa-twitter"></i>
								</a>
							</div>
							<div class="float_left">
								<a href="">
									<i class="fa fa-dribbble"></i>
								</a>
							</div>
							<div class="float_left">
								<a href="">
									<i class="fa fa-behance"></i>
								</a>
							</div>
						</div>
						<div class="clear"></div>
						<div class="fco margin_bottom">
							web designer
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div data-value="popup2" class="sect2_team_img2 margin_bottom"></div>
					<div>
						<div class="float_left subtitlu">
							Mark Doe
						</div>
						<div class="float_right">
							<div class="float_left">
								<a href="">
									<i class="fa fa-twitter"></i>
								</a>
							</div>
							<div class="float_left">
								<a href="">
									<i class="fa fa-dribbble"></i>
								</a>
							</div>
							<div class="float_left">
								<a href="">
									<i class="fa fa-behance"></i>
								</a>
							</div>
						</div>
						<div class="clear"></div>
						<div class="fco margin_bottom">
							web designer
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div data-value="popup3" class="sect2_team_img3 margin_bottom"></div>
					<div>
						<div class="float_left subtitlu">
							Alex Doe
						</div>
						<div class="float_right">
							<div class="float_left">
								<a href="">
									<i class="fa fa-twitter"></i>
								</a>
							</div>
							<div class="float_left">
								<a href="">
									<i class="fa fa-dribbble"></i>
								</a>
							</div>
							<div class="float_left">
								<a href="">
									<i class="fa fa-behance"></i>
								</a>
							</div>
						</div>
						<div class="clear"></div>
						<div class="fco margin_bottom">
							web designer
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune3_team">
		<div class="row">
			<div class="col-md-3 co pos_relative sect3_col">
				<div class="titlu margin_bottom">
					Inspiring
				</div>
				<div>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry.
				</div>
				<div class="sect3_contact_i">
					<i class="fa fa-map-marker"></i>
				</div>
			</div>
			<div class="col-md-3 cn pos_relative sect3_col">
				<div class="titlu margin_bottom">
					Empowering
				</div>
				<div>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry.
				</div>
				<div class="sect3_contact_i">
					<i class="fa fa-envelope"></i>
				</div>
			</div>
			<div class="col-md-3 clb pos_relative sect3_col">
				<div class="titlu margin_bottom">
					Rewarding
				</div>
				<div>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry.
				</div>
				<div class="sect3_contact_i">
					<i class="fa fa-phone"></i>
				</div>
			</div>
			<div class="col-md-3 cp pos_relative sect3_col">
				<div class="titlu margin_bottom">
					and Fun
				</div>
				<div>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry.
				</div>
				<div class="sect3_contact_i">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
		</div>
	</div>



	<?php
	include ("/elements/proiect_footer.php");
	?>
	

	<?php
	include ("/elements/html2_proiect.php");
	?>