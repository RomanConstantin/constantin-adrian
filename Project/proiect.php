	<?php
	include ("/elements/html1_proiect.php");
	?>

	<div class="settings">
		<div>
			<i class="fa fa-cogs cursor"></i>
		</div>
		<div class="disp_none sett">
			<i class="fa fa-facebook-f"></i>
		</div>
		<div class="disp_none sett">
			<i class="fa fa-twitter"></i>
		</div>
		<div class="disp_none sett">
			<i class="fa fa-instagram"></i>
		</div>
	</div>
	<?php
	include ("/elements/scroll_to_top.php");
	?>
	<div class="container-fluid header sectiune1">
		<div class="container">
			<div class="row">
				<div class="header_left float_left">
					<div class="float_left">
						<a href="">About</a>
					</div>
					<div class="float_left">
						<a href="">Features</a>
					</div>
					<div class="float_left">
						<a href="">Pricing</a>
					</div>
					<div class="float_left">
						<a href="">Terms</a>
					</div>
				</div>
				<div class="header_right float_right">
					<div class="float_left">
						<a href="">
							<i class="fa fa-twitter"></i>
						</a>
					</div>
					<div class="float_left">
						<a href="">
							<i class="fa fa-facebook-f"></i>
						</a>
					</div>
					<div class="float_left">
						<a href="">
							<i class="fa fa-instagram"></i>
						</a>
					</div>
					<div class="float_left">
						<a href="">
							<i class="fa fa-dribbble"></i>
						</a>
					</div>
					<div class="float_left">
						<a href="">
							<i class="fa fa-behance"></i>
						</a>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune2">
		<div class="slider">
			<div class="slider_activ">
				<div>
					<img src="/web/proiect/img/bg15.jpg">
					<div class="fade1"></div>
					<div class="text_burger">
						<div>
							All you need 
						</div>
						<div>
							just one template
						</div>
					</div>
					<div class="text_slider">
						<div>
							<h2>Free updates</h2>
							<h2>Top notch Support</h2>
						</div>
						<div>
							AND SO MUCH MORE...
						</div>
						<div class="see_features">
							<a href="">SEE FEATURES</a>
						</div>
					</div>
				</div>
				<div>
					<img src="/web/proiect/img/bg11.jpg">
					<div class="fade1"></div>
					<div class="text_burger">
						<div>
							All you need 
						</div>
						<div>
							just one template
						</div>
					</div>
					<div class="text_slider">
						<div>
							<h2>Free updates</h2>
							<h2>Top notch Support</h2>
						</div>
						<div>
							AND SO MUCH MORE...
						</div>
						<div class="see_features">
							<a href="">SEE FEATURES</a>
						</div>
					</div>
				</div>
				<div>
					<img src="/web/proiect/img/bg14.jpg">
					<div class="fade1"></div>
					<div class="text_burger">
						<div>
							All you need 
						</div>
						<div>
							just one template
						</div>
					</div>
					<div class="text_slider">
						<div>
							<h2>Free updates</h2>
							<h2>Top notch Support</h2>
						</div>
						<div>
							AND SO MUCH MORE...
						</div>
						<div class="see_features">
							<a href="">SEE FEATURES</a>
						</div>
					</div>
				</div>
			</div>
			<?php
				include ("/elements/meniu.php");
			?>
		</div>
	</div>
	<div class="container-fluid sectiune3">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="text-center sect3_img">
						<i class="fa fa-mobile"></i>
					</div>
					<div class="text-center subtitlu">
						Responsive Design
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
				</div>
				<div class="col-md-4">
					<div class="text-center sect3_img">
						<i class="fa fa-gift"></i>
					</div>
					<div class="text-center subtitlu">
						Premium Plugins
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
				</div>
				<div class="col-md-4">
					<div class="text-center sect3_img">
						<i class="fa fa-star"></i>
					</div>
					<div class="text-center subtitlu">
						Free Updates
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune4">
		 <div class="row">
		 	<div class="col-md-6 sect4_img">
		 	 	<div class="fade1"></div>
		 	</div>
		 	<div class="col-md-6 sect4_text">
		 		<div class="titlu">
		 			About us
		 		</div>
		 		<div class="line2 line3"></div>
		 		<div class="margin_bottom">
		 			Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry. 
		 			Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
		 		</div>
		 		<div class="subtitlu margin_bottom">
		 			<i class="fa fa-heart"></i>
		 			Happy clients
		 		</div>
		 		<div>
		 			Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
		 		</div>
		 	</div>
		 </div>
	</div>
	<div class="clear"></div>
	<div class="container-fluid sectiune5">
		<div class="container">
			<div class="row">
				<div class="col-md-6 margin_top  col-xs-12 col-sm-12">
					<div class="col-md-2 col-xs-2 col-sm-2 sect5 text-center">
						<i class="fa fa-life-ring"></i>
					</div>
					<div class="col-md-10 col-xs-10 col-sm-10">
						<div class="subtitlu">
							Top notch Support
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="col-md-6 margin_top  col-xs-12 col-sm-12">
					<div class="col-md-2 col-xs-2 col-sm-2 sect5 text-center">
						<i class="fa fa-align-right"></i>
					</div>
					<div class="col-md-10 col-xs-10 col-sm-10">
						<div class="subtitlu">
							SEO Ready
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="col-md-6 margin_top  col-xs-12 col-sm-12">
					<div class="col-md-2 col-xs-2 col-sm-2 sect5 text-center">
						<i class="fa fa-check-square"></i>
					</div>
					<div class="col-md-10 col-xs-10 col-sm-10">
						<div class="subtitlu">
							Bootstrap Based
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="col-md-6 margin_top col-xs-12 col-sm-12">
					<div class="col-md-2 col-xs-2 col-sm-2 sect5 text-center">
						<i class="fa fa-chrome"></i>
					</div>
					<div class="col-md-10 col-xs-10 col-sm-10">
						<div class="subtitlu">
							Working widgets
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune6">
		<div class="row">
			<div class="col-md-4 sect6 cb">
				<div class="sect6_abs">1</div>
				<div class="titlu margin_bottom">
					Customizable
				</div>
				<div>
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
			</div>
			<div class="col-md-4 sect6 cn">
				<div class="sect6_abs">2</div>
				<div class="titlu margin_bottom">
					Customizable
				</div>
				<div>
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
			</div>
			<div class="col-md-4 sect6 co">
				<div class="sect6_abs">3</div>
				<div class="titlu margin_bottom">
					Customizable
				</div>
				<div>
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune7">
		<div class="container">
			<div class="row">
				<div>
					<div class="float_left">
						<div class="titlu">
							Our Projects
						</div>
						<div class="line2 line3"></div>
					</div>
					<div class="float_right sect7_responsive">
						<div class="sect7_right fco float_left sect7_responsive_bars">
							<i class="fa fa-bars"></i>
						</div>
						<div class="sect7_right float_left">
							<a href="">Above</a>
						</div>
						<div class="sect7_right float_left">
							/
						</div>
						<div class="sect7_right float_left">
							<a href="">Beyond</a>
						</div>
						<div class="sect7_right float_left">
							/
						</div>
						<div class="sect7_right float_left">
							<a href="">Nowhere</a>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="sect7_img_home">
					<div class="col-md-4 sect7_img1 pos_relative sect7_over">
						<div class=" sect7_img">
							<div class="opac_our_projects no_padding"></div>
							<div class="titlu pos_relative">
								Design, photography
							</div>
							<div class="line2 line3 margin_bottom pos_relative"></div>
							<div class="pos_relative">
								Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
							</div>
						</div>
					</div>
					<div class="col-md-4 sect7_img2 pos_relative sect7_over">
						<div class=" sect7_img">
							<div class="opac_our_projects no_padding"></div>
							<div class="titlu pos_relative">
								Design, photography
							</div>
							<div class="line2 line3 margin_bottom pos_relative"></div>
							<div class="pos_relative">
								Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
							</div>
						</div>
					</div>
					<div class="col-md-4 sect7_img3 pos_relative sect7_over">
						<div class=" sect7_img">
							<div class="opac_our_projects no_padding"></div>
							<div class="titlu pos_relative">
								Design, photography
							</div>
							<div class="line2 line3 margin_bottom pos_relative"></div>
							<div class="pos_relative">
								Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
							</div>
						</div>
					</div>
					<div class="col-md-4 sect7_img4 pos_relative sect7_over">
						<div class=" sect7_img">
							<div class="opac_our_projects no_padding"></div>
							<div class="titlu pos_relative">
								Design, photography
							</div>
							<div class="line2 line3 margin_bottom pos_relative"></div>
							<div class="pos_relative">
								Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
							</div>
						</div>
					</div>
					<div class="col-md-4 sect7_img5 pos_relative sect7_over">
						<div class=" sect7_img">
							<div class="opac_our_projects no_padding"></div>
							<div class="titlu pos_relative">
								Design, photography
							</div>
							<div class="line2 line3 margin_bottom pos_relative"></div>
							<div class="pos_relative">
								Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
							</div>
						</div>
					</div>
					<div class="col-md-4 sect7_img6 pos_relative sect7_over">
						<div class=" sect7_img">
							<div class="opac_our_projects no_padding"></div>
							<div class="titlu pos_relative">
								Design, photography
							</div>
							<div class="line2 line3 margin_bottom pos_relative"></div>
							<div class="pos_relative">
								Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune8 pos_relative">
		<div class="row">
			<div class="sect8_part1">
				<div class="text-center titlu">
					The team behind all this
				</div>
				<div class="line line2"></div>
			</div>
			<div class="slider2">
				<div class="float_left">
					<img src="/web/proiect/img/team2.jpg" height="250px">
				</div>
				<div class="float_left">
					<img src="/web/proiect/img/team3.jpg" height="250px">
				</div>
				<div class="float_left">
					<img src="/web/proiect/img/team5.jpg" height="250px">
				</div>
				<div class="float_left">
					<img src="/web/proiect/img/team2.jpg" height="250px">
				</div>
				<div class="float_left">
					<img src="/web/proiect/img/team3.jpg" height="250px">
				</div>
				<div class="float_left">
					<img src="/web/proiect/img/team2.jpg" height="250px">
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune9">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-xs-6 col-sm-6">
					<img src="/web/proiect/img/client01a.png">
				</div>
				<div class="col-md-3 col-xs-6 col-sm-6">
					<img src="/web/proiect/img/client02a.png">
				</div>
				<div class="col-md-3 col-xs-6 col-sm-6">
					<img src="/web/proiect/img/client03a.png">
				</div>
				<div class="col-md-3 col-xs-6 col-sm-6">
					<img src="/web/proiect/img/client04a.png">
				</div>
				<div class="col-md-3 col-xs-6 col-sm-6">
					<img src="/web/proiect/img/client05a.png">
				</div>
				<div class="col-md-3 col-xs-6 col-sm-6">
					<img src="/web/proiect/img/client06a.png">
				</div>
				<div class="col-md-3 col-xs-6 col-sm-6">
					<img src="/web/proiect/img/client07a.png">
				</div>
				<div class="col-md-3 col-xs-6 col-sm-6">
					<img src="/web/proiect/img/client08a.png">
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune10">
		<div class="row">
			<div class="col-md-8 sect10_part1">
				<div class="text-center">
					<i>Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.</i>
				</div>
				<div class="text-center margin_top">
					<b>Mark Doe</b>
				</div>
			</div>
			<div class="col-md-4 sect10_part2">
				<div class="line2 line3"></div>
				<div class="subtitlu">
					Testimonials
				</div>
				<div>
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
				<div class="quote">
					<i class="fa fa-quote-right"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune11">
		<div class="container">
			<div class="row">
				<div class="col-md-4 sect11">
					<div class="subtitlu">
						<i class="fa fa-life-ring"></i>
						Top notch Support
					</div>
					<div>
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
					<div class="line2 line3"></div>
				</div>
				<div class="col-md-4 sect11">
					<div class="subtitlu">
						<i class="fa fa-shopping-cart"></i>
						Ecommerce
					</div>
					<div>
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
					<div class="line2 line3"></div>
				</div>
				<div class="col-md-4 sect11">
					<div class="subtitlu">
						<i class="fa fa-exclamation-circle"></i>
						Reliable
					</div>
					<div>
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
					<div class="line2 line3"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune12">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="subtitlu">
						Palas
					</div>
					<div class="line2 line3"></div>
					<div class="margin_top sect12_part1">
						<div class="float_left sect12_list1">
							<a href="">Home</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Contact</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Privacy Policy</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Services</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Terms</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Security</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Pricing</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Features</a>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="col-md-4">
					<div class="subtitlu">
						Get social
					</div>
					<div class="line2 line3"></div>
					<div class="margin_top">
						<div class="sect12_list2 float_left">
							<a href="">
								<i class="fa fa-twitter"></i>
							</a>
						</div>
						<div class="sect12_list2 float_left">
							<a href="">
								<i class="fa fa-facebook-f"></i>
							</a>
						</div>
						<div class="sect12_list2 float_left">
							<a href="">
								<i class="fa fa-google-plus"></i>
							</a>
						</div>
						<div class="sect12_list2 float_left">
							<a href="">
								<i class="fa fa-pinterest"></i>
							</a>
						</div>
						<div class="sect12_list2 float_left">
							<a href="">
								<i class="fa fa-instagram"></i>
							</a>
						</div>
						<div class="sect12_list2 float_left">
							<a href="">
								<i class="fa fa-dribbble"></i>
							</a>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="col-md-4">
					<div class="subtitlu">
						Tweets
					</div>
					<div class="line2 line3"></div>
					<div class="margin_top">
						<div class="col-md-1 sect12_part3">
							<i class="fa fa-twitter"></i>
						</div>
						<div class="col-md-11 sect12_text">
							<a href="">@abusinesstheme</a> Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
							<div class="hours">
								22 hours ago
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="col-md-4 col-md-offset">
					<div class="email">
						<input type="text" placeholder="Type email and hit enter" name="email">
					</div>
				</div>
			</div>
		</div>
	</div>


	<?php
	include ("/elements/html2_proiect.php");
	?>
