	<?php
	include ("/elements/html1_proiect.php");
	?>

	

	<?php
	include ("/elements/proiect_header.php");
	?>

	<div class="container-fluid sectiune2_services">
		<div class="opac_contact"></div>
		<div class="container pos_relative">
			<div class="row">
				<div class="titlu titlu_about">
					Services
				</div>
				<div class="line2 line3"></div>
				<div class="col-md-4 sect2_about_text">
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
			</div>
		</div>
		<?php
			include ("/elements/meniu.php");
		?>
	</div>
	<div class="clear"></div>
	<div class="container-fluid sectiune3_services">
		<div class="container">
			<div class="row">
				<div class="col-md-4" style="border-right: 1px solid #667;">
					<div class="text-center sect3_img">
						<i class="fa fa-mobile"></i>
					</div>
					<div class="text-center subtitlu">
						Responsive Design
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
				</div>
				<div class="col-md-4" style="border-right: 1px solid #667;">
					<div class="text-center sect3_img">
						<i class="fa fa-gift"></i>
					</div>
					<div class="text-center subtitlu">
						Premium Plugins
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
				</div>
				<div class="col-md-4">
					<div class="text-center sect3_img">
						<i class="fa fa-star"></i>
					</div>
					<div class="text-center subtitlu">
						Free Updates
					</div>
					<div class="line line2"></div>
					<div class="text-center">
						Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune4_about">
		<div class="row">
			<div class="col-md-4 sect4_about1">
				<div class="fade1"></div>
				<div class="titlu ">
					Work Hard
				</div>
				<div class="line2 line3"></div>
				<div>
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
			</div>
			<div class="col-md-4 sect4_about2">
				<div class="fade1"></div>
				<div class="titlu ">
					Play Hard
				</div>
				<div class="line2 line3"></div>
				<div>
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
			</div>
			<div class="col-md-4 sect4_about3">
				<div class="fade1"></div>
				<div class="titlu ">
					and Have Fun
				</div>
				<div class="line2 line3"></div>
				<div>
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid sectiune5_services">
		<div class="container">
			<div class="row">
				<div class="col-md-6 margin_top">
					<div class="col-md-2 sect5 text-center fco">
						<i class="fa fa-mobile"></i>
					</div>
					<div class="col-md-10">
						<div class="subtitlu">
							Responsive Design
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="col-md-6 margin_top">
					<div class="col-md-2 sect5 text-center fco">
						<i class="fa fa-check-square"></i>
					</div>
					<div class="col-md-10">
						<div class="subtitlu">
							Bootstrap Based
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="col-md-6 margin_top">
					<div class="col-md-2 sect5 text-center fco">
						<i class="fa fa-percent"></i>
					</div>
					<div class="col-md-10">
						<div class="subtitlu">
							Working widgets
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="col-md-6 margin_top">
					<div class="col-md-2 sect5 text-center fco">
						<i class="fa fa-wrench"></i>
					</div>
					<div class="col-md-10">
						<div class="subtitlu">
							Costumizable
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="col-md-6 margin_top">
					<div class="col-md-2 sect5 text-center fco">
						<i class="fa fa-shopping-cart"></i>
					</div>
					<div class="col-md-10">
						<div class="subtitlu">
							Ecommerce
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
				<div class="col-md-6 margin_top">
					<div class="col-md-2 sect5 text-center fco">
						<i class="fa fa-chrome"></i>
					</div>
					<div class="col-md-10">
						<div class="subtitlu">
							Reliable
						</div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<?php
	include ("/elements/proiect_footer.php");
	?>
	
	<?php
	include ("/elements/html2_proiect.php");
	?>