$(document).ready(function(){

	$('.slider_activ').slick(
	  {
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  autoplay: true,
	  autoplaySpeed: 2000,
	  arrows: false,
	  dots: false,
	  }
	);

	$('.slick-next').html('<i class="fa fa-arrow-right" aria-hidden="true"></i>');
	$('.slick-prev').html('<i class="fa fa-arrow-left" aria-hidden="true"></i>');
	$('.slick-dots li').html('<i class="fa fa-circle-thin" aria-hidden="true"></i>');

	$('.slider2').slick(
	{
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  autoplay: true,
	  autoplaySpeed: 2000,
	  arrows: false,
	  dots: false,
	  responsive: [	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	}
	);

	$('.sect7_over').mouseover(function(){
		$(this).find('.sect7_img').fadeIn(200);
	});
	$('.sect7_over').mouseleave(function(){
		$(this).find('.sect7_img').fadeOut(200);
	});
	
	$('.scroll').click(function(){
		$("html, body").animate({ scrollTop: 0 }, "5000");
	});
	$(document).scroll(function(){
		if ($(document).scrollTop() >= 200) {
			$('.scroll').fadeIn();
		}
		else {
			$('.scroll').fadeOut();
		}
	});

	$('.submit_checkbox').click(function(){
		var nume = $('.nume').val();
		var prenume = $('.prenume').val();
		var subject = $('.subject').val();
		var email_contact = $('.email_contact').val();
		var textarea = $('.textarea').val();
		var checkbox_contact = $('.checkbox_contact').val();

		if (nume != '' && prenume != '' && subject != '' && email_contact != '' && textarea != '') 
		{
			$.ajax(
			{
				url: "/trimite.php", //url catre fisierul php
				data:
				{
					nume : nume,
					prenume : prenume,
					subject : subject,
					email_contact : email_contact,
					textarea : textarea,
					checkbox_contact : checkbox_contact,
				},
				method: 'POST',
				succes: function(data)
				{
					alert('succes!');

					$('.nume').val("");
					$('.prenume').val("");
					$('.telefon').val("");
					$('.email_contact').val("");
					$('.selecteaza_departament').val("departament info");
					$('.mesaj').val("");
					$('.checkbox:checked').val("");
				
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					alert('Error: '+jqXHR.status);
				}
			});
		}
		else
		{
			alert('Va rugam completati toate campurile!')
		}
		
	});

	$('.close-this').click(function(){
		$(".popup").fadeOut();
	});
	$('.close-this').click(function(){
		$(".popup2").fadeOut();
	});
	$('.close-this').click(function(){
		$(".popup3").fadeOut();
	});

	$('.sect2_team_img1').click(function() {
		$(this).find('.popup').fadeIn(1000);
		var x = $(this).attr('data-value');
		$('.'+x).fadeIn();

	});
	$('.sect2_team_img2').click(function() {
		$(this).find('.popup2').fadeIn(1000);
		var x = $(this).attr('data-value');
		$('.'+x).fadeIn();

	});
	$('.sect2_team_img3').click(function() {
		$(this).find('.popup3').fadeIn(1000);
		var x = $(this).attr('data-value');
		$('.'+x).fadeIn();

	});
	$('.faq_div1').click(function(){
		if ($(this).find('.fa-angle-down').hasClass('angle_up')) {
			$('.faq_div1 .fa-angle-down').removeClass('angle_up');
			$('.faq_div1 .fa-angle-up').addClass('angle_up');
			$('.sect3_faq_text1').slideUp('fast');
		}
		else {
			$('.faq_div1 .fa-angle-down').removeClass('angle_up');
			$('.faq_div1 .fa-angle-up').addClass('angle_up');
			$(this).find('.fa').toggleClass('angle_up');
			$(this).next('div').slideToggle();
			$(this).parent().siblings().children().next().slideUp();
		}

	});

	$('.faq_marg').click(function(){
		$('.chg').hide();
		var x = $(this).attr('data-value');
		$('.'+x).fadeIn(500);
		$('.faq_div1').show();
	});

	$('.settings').click(function(){
		$(this).find('.sett').slideToggle();
	});

	$('.hover_servicii').mouseover(function(){
		$(this).find('.servicii_list').slideDown();
	});
	$('.servicii_list').mouseleave(function(){
		$(this).slideUp();
	});

	$('.hover_servicii').mouseover(function(){
		$(this).find('.burger_servicii_list').slideDown();
	});
	$('.burger_servicii_list').mouseleave(function(){
		$(this).slideUp();
	});

	$('.burger_mobile').click(function(){
		$('.burger_meniu').slideToggle();
	});

});