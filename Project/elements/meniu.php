<div class="meniu">
	<div class="container">
		<div class="row">
			<div>
				<div class="logo float_left">
					<img src="/web/proiect/img/logo-light.png">
				</div>
				<div class="float_right meniu_list">
					<div class="float_left">
						<a href="/web/proiect/proiect.php">Home</a>
					</div>
					<div class="float_left">
						<a href="/web/proiect/about_us_proiect.php">About us</a>
					</div>
					<div class="float_left">
						<a href="/web/proiect/portofolio_proiect.php">Portofolio</a>
					</div>
					<div class="float_left hover_servicii">
						<a href="">Services</a>
						<ul class="servicii_list">
							<div class="services_line"></div>
							<li>
								<a href="/web/proiect/faq_proiect.php" style="margin-left: 0;">FAQ</a>
							</li>
							<li>
								<a href="/web/proiect/team_proiect.php" style="margin-left: 0;">Team</a>
							</li>
						</ul>
					</div>
					<div class="float_left">
						<a href="/web/proiect/contact_proiect.php">Contact</a>
					</div>
				</div>
				<div class="burger_mobile float_right">
					<div>
						<i class="fa fa-bars"></i>
					</div>
				</div>
				<div class="burger_meniu">
					<div class="">
						<a href="/web/proiect/proiect.php">Home</a>
					</div>
					<div class="">
						<a href="/web/proiect/faq_proiect.php">About us</a>
					</div>
					<div class="">
						<a href="/web/proiect/portofolio_proiect.php">Portofolio</a>
					</div>
					<div class="hover_servicii">
						<a href="/web/proiect/services_proiect.php">Services</a>
						<ul class="burger_servicii_list">
							<li>
								<a href="/web/proiect/faq_proiect.php" style="margin-left: 0;">FAQ</a>
							</li>
							<li>
								<a href="/web/proiect/team_proiect.php" style="margin-left: 0;">Team</a>
							</li>
						</ul>
					</div>
					<div class="float_left">
						<a href="/web/proiect/contact_proiect.php">Contact</a>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>