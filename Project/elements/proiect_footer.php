<div class="container-fluid sectiune12">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="subtitlu">
						Palas
					</div>
					<div class="line2 line3"></div>
					<div class="margin_top sect12_part1">
						<div class="float_left sect12_list1">
							<a href="">Home</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Contact</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Privacy Policy</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Services</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Terms</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Security</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Pricing</a>
						</div>
						<div class="float_left sect12_list1">
							<a href="">Features</a>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="col-md-4">
					<div class="subtitlu">
						Get social
					</div>
					<div class="line2 line3"></div>
					<div class="margin_top">
						<div class="sect12_list2 float_left">
							<a href="">
								<i class="fa fa-twitter"></i>
							</a>
						</div>
						<div class="sect12_list2 float_left">
							<a href="">
								<i class="fa fa-facebook-f"></i>
							</a>
						</div>
						<div class="sect12_list2 float_left">
							<a href="">
								<i class="fa fa-google-plus"></i>
							</a>
						</div>
						<div class="sect12_list2 float_left">
							<a href="">
								<i class="fa fa-pinterest"></i>
							</a>
						</div>
						<div class="sect12_list2 float_left">
							<a href="">
								<i class="fa fa-instagram"></i>
							</a>
						</div>
						<div class="sect12_list2 float_left">
							<a href="">
								<i class="fa fa-dribbble"></i>
							</a>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="col-md-4">
					<div class="subtitlu">
						Tweets
					</div>
					<div class="line2 line3"></div>
					<div class="margin_top">
						<div class="col-md-1 sect12_part3">
							<i class="fa fa-twitter"></i>
						</div>
						<div class="col-md-11 sect12_text">
							<a href="">@abusinesstheme</a> Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
							<div class="hours">
								22 hours ago
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="col-md-4 col-md-offset">
					<div class="email">
						<input type="text" placeholder="Type email and hit enter" name="email">
					</div>
				</div>
			</div>
		</div>
	</div>