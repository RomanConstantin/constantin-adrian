	<?php
	include ("/elements/html1_proiect.php");
	?>

	

	<?php
	include ("/elements/proiect_header.php");
	?>

	<div class="container-fluid sectiune2_faq">
		<div class="opac_contact"></div>
		<div class="container pos_relative">
			<div class="row">
				<div class="titlu titlu_about">
					FAQs
				</div>
				<div class="line2 line3"></div>
				<div class="col-md-4 sect2_about_text">
					Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
				</div>
				<div class="meniu_faq">
					<div data-value="sect3_faq_part1" class="float_left meniu_faq_list faq_marg cursor">
						
							Account Set Up
						
					</div>
					<div data-value="sect3_faq_part2" class="float_left meniu_faq_list faq_marg cursor">
						
							Buying
						
					</div>
					<div data-value="sect3_faq_part3" class="float_left meniu_faq_list faq_marg cursor">
						
							Receive Payments
						
					</div>
					<div data-value="sect3_faq_part4" class="float_left faq_marg cursor">
						
							Using the card
						
					</div>
				</div>
			</div>
		</div>
		<?php
			include ("/elements/meniu.php");
		?>
	</div>
	<div class="clear"></div>
	<div class="container-fluid sectiune3_faq">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<div class="sect3_faq_part1 chg">
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down cursor"></i> 
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								How to create an account
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								Account details
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								How to change username and password
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								I forgot the username
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								Account removal
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
					</div>
					<div class="sect3_faq_part2 disp_none chg">
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down cursor"></i> 
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								How to create an account
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								Account details
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								How to change username and password
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								I forgot the username
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								Account removal
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
					</div>
					<div class="sect3_faq_part3 disp_none chg">
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down cursor"></i> 
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								How to create an account
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								Account details3
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								How to change username and password3
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								I forgot the username3
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								Account removal3
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
					</div>
					<div class="sect3_faq_part4 disp_none chg">
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down cursor"></i> 
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								How to create an account4
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								Account details4
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								How to change username and password4
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								I forgot the username4
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
						<div class="margin_bottom faq_div1">
							<div class="col-md-1">
								<i class="fa fa-angle-down"></i>
								<i class="fa fa-angle-up angle_up cursor"></i>
							</div>
							<div class="subtitlu cursor">
								Account removal4
							</div>
						</div>
						<div class="col-md-offset-1 margin_bottom sect3_faq_text1">
							Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and simply dummy text of the printing and typesetting industry.
						</div>
					</div>
					<div class="sect3_faq_part1">
						<div class="subtitlu">
							Couldn't find what you are looking for?
						</div>
						<div class="line2 line3"></div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
						</div>
						<div class="faq_contact">
							<a href="">
								Contact us
							</a>
						</div>
					</div>
					<div>
						<div class="col-md-6 sect11">
							<div class="subtitlu">
								<i class="fa fa-life-ring"></i>
								Premium Support
							</div>
							<div>
								Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
							</div>
							<div class="line2 line3"></div>
						</div>
						<div class="col-md-6 sect11">
							<div class="subtitlu">
								<i class="fa fa-globe"></i>
								Customizable
							</div>
							<div>
								Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry.
							</div>
							<div class="line2 line3"></div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="sect3_faq_part1">
						<div class="subtitlu">
							Above and beyond
						</div>
						<div class="line2 line3"></div>
						<div>
							Lorem Ipsum is simply dummy simply dummy text of the printing and typesetting industry. 
						</div>
					</div>
					<div class="sect3_faq_part1">
						<div class="subtitlu margin_bottom">
							Categories
						</div>
						<div style="margin-bottom: 10px;">
							<div class="col-md-1 no_padding float_left">
								<i class="fa fa-angle-right"></i>
							</div>
							<div>
								Account details
							</div>
						</div>
						<div style="margin-bottom: 10px;">
							<div class="col-md-1 no_padding float_left">
								<i class="fa fa-angle-right"></i>
							</div>
							<div>
								Software Engineering
							</div>
						</div>
						<div style="margin-bottom: 10px;">
							<div class="col-md-1 no_padding float_left">
								<i class="fa fa-angle-right"></i>
							</div>
							<div>
								Web Graphic
							</div>
						</div>
						<div style="margin-bottom: 10px;">
							<div class="col-md-1 no_padding float_left">
								<i class="fa fa-angle-right"></i>
							</div>
							<div>
								Web Programming
							</div>
						</div>
						<div style="margin-bottom: 10px;">
							<div class="col-md-1 no_padding float_left">
								<i class="fa fa-angle-right"></i>
							</div>
							<div>
								Software Design
							</div>
						</div>
					</div>
					<div class="sect3_faq_part1">
						<div class="subtitlu margin_bottom">
							Tags
						</div>
						<div class="faq_tags">
							<a href="" class="float_left" style="margin-right: 10px; margin-bottom: 10px;">
								Wordpress
							</a>
							<a href="" class="float_left" style="margin-right: 10px; margin-bottom: 10px;">
								Concrete5
							</a>
							<a href="" class="float_left" style="margin-right: 10px; margin-bottom: 10px;">
								Drupal
							</a>
							<a href="" class="float_left" style="margin-right: 10px; margin-bottom: 10px;">
								Joomia
							</a>
							<a href="" class="float_left" style="margin-right: 10px; margin-bottom: 10px;">
								PHP
							</a>
							<a href="" class="float_left" style="margin-right: 10px; margin-bottom: 10px;">
								HTML5
							</a>
							<a href="" class="float_left" style="margin-right: 10px; margin-bottom: 10px;">
								CSS3
							</a>
							<a href="" class="float_left" style="margin-right: 10px; margin-bottom: 10px;">
								jQuery
							</a>
							<a href="" class="float_left" style="margin-right: 10px; margin-bottom: 10px;">
								Java
							</a>
							<a href="" class="float_left" style="margin-right: 10px; margin-bottom: 10px;">
								Ruby
							</a>
							<a href="" class="float_left" style="margin-right: 10px; margin-bottom: 10px;">
								Javascript
							</a>
						</div>
					</div>
					<div class="clear"></div>
					<div class="faq_tags">
						<div class="subtitlu margin_top">
							Get social
						</div>
						<div class="line2 line3"></div>
						<div class="col-md-1 no_padding">
							<a href="">
								<i class="fa fa-twitter"></i>
							</a>
						</div>
						<div class="col-md-11 sect4_contact_social_1 no_padding">
							Follow us
						</div>
						<div class="clear"></div>
						<div class="col-md-1 no_padding">
							<a href="">
								<i class="fa fa-facebook-f"></i>
							</a>
						</div>
						<div class="col-md-11 sect4_contact_social_2 no_padding">
							Like us
						</div>
						<div class="clear"></div>
						<div class="col-md-1 no_padding">
							<a href="">
								<i class="fa fa-envelope"></i>
							</a>
						</div>
						<div class="col-md-11 sect4_contact_social_3 no_padding">
							Subscribe
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>



	<?php
	include ("/elements/proiect_footer.php");
	?>
	
	<?php
	include ("/elements/html2_proiect.php");
	?>